function createCard(title, description, pictureUrl, dateStarts, dateEnds, location) { //the ' is a template literal
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle">${location}</h6>

          <p class="card-text">${description}</p>
        </div>
      </div>
      <div class="card">
      <div class ="card-body">
      <p class ="card-text">${dateStarts} - ${dateEnds}</p>
      <div>
      </div>
    `;
  }
  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
      const response = await fetch(url);
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const dateStarts = new Date(details.conference.starts);
            const newDate = dateStarts.toLocaleDateString()
            const dateEnds = new Date(details.conference.ends);
            const newDateEnds = dateEnds.toLocaleDateString()
            const html = createCard(title, description, pictureUrl, newDate, newDateEnds, location);
            const column = document.querySelector('.col');
  column.innerHTML += html;
          }
        }
      }
    } catch (e) {
      // Figure out what to do if an error is raised
    }
  });